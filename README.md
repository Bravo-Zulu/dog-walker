Javiscript String Combination Experiment

User Story:
Write a JavaScript function that generates all combinations of a string. 
function input: string 
function return: Array of strings.
Example string : 'dog' 
Expected Output : ['d','o','do','g','dg','og','dog’]

Contact: coden@bravozulu.bz
