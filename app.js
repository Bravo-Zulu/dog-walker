// User Story:
//Write a JavaScript function that generates all combinations of a string. 
//function input: string 
//function return: Array of strings.
//Example string : 'dog' 
//Expected Output : ['d','o','do','g','dg','og','dog’]
//Curtis' Solution

// walkTheDog is the primary worker 
var walkTheDog = function (walkingThisDog) {
        var frankenFurter = new Array(); //our destination array
        //frankeFiller is a sub function checks to see if the value is already in the array, and if not, adds it
        var frankenFiller = function (puppy) {
            if ((frankenFurter.indexOf(puppy) === -1)) {
                if (puppy !== '') {
                    frankenFurter.push(puppy);
                }
            }
        }
        var dogParts = walkingThisDog.split("");
        var hotdog = "";
        for (var leg = 0; leg < dogParts.length; leg++) {
            frankenFiller(hotdog);
            hotdog = hotdog + dogParts[leg];
            frankenFiller(hotdog);
            for (var tail = leg + 1; tail < dogParts.length; tail++) {
                frankenFiller(hotdog);
                hotdog = hotdog + dogParts[tail];
                frankenFiller(hotdog);
            }
           // hotdog = "";
        }
        return frankenFurter;
    }

    // I added support reverse string they are valid combinations of the string, even though the User Story only calls for the forward case.
    // Because there's no benefit in reinventing the wheel, I useed this perfectly good code:
    //SOURCE: https://medium.freecodecamp.org/how-to-reverse-a-string-in-javascript-in-3-different-ways-75e4763c68cb
var reverseString = function (str) {
    if (str === "") // This is the terminal case that will end the recursion
        return "";
    else return reverseString(str.substr(1)) + str.charAt(0);
}

//We also want to combine these to strings together, but contact will create duplicates. 
//Fortunately, others have created a perfectly good solution
//https://stackoverflow.com/questions/1584370/how-to-merge-two-arrays-in-javascript-and-de-duplicate-items
Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
};

var mydog = "dog"; //default config
var godym = reverseString(mydog); //reversing the dogs.
var poundPuppies = walkTheDog(mydog).concat(walkTheDog(godym)).unique();

console.log(poundPuppies); //Who let the dogs out?